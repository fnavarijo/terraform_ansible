provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_droplet" "abcelectronica_prod" {
  image    = "ubuntu-18-04-x64"
  name     = "ABCProdServer"
  region   = "nyc3"
  size     = "s-1vcpu-2gb"
  ssh_keys = ["${var.ssh_fingerprint}"]

  provisioner "remote-exec" {
    inline = ["apt-get -y update && apt-get install -y python"]

    connection {
      type        = "ssh"
      user        = "root"
      private_key = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -i '${self.ipv4_address},' --private-key ${var.ssh_key_private} provision.yml"
  }
}

# provisioner "local-exec" {}

